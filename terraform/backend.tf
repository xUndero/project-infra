terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "otus-kuber-2020-04"

    workspaces {
      name = "test_k8s"
    }
  }
}
