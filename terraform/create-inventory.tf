resource "null_resource" "ansible-provision" {

  depends_on = [google_compute_instance.master, google_compute_instance.worker, google_compute_instance.balancer]

  ##Create Balancer Inventory
  provisioner "local-exec" {
    command = "echo \"[balancers]\" > ./inventory.ini"
  }

  provisioner "local-exec" {
    command = "echo \"${google_compute_instance.balancer.name} \"ansible_host=\"${google_compute_instance.balancer.network_interface.0.access_config.0.nat_ip}\" >> ./inventory.ini"
  }

  ##Create Masters Inventory
  provisioner "local-exec" {
    command = "echo \"\n[kube-master]\" >> ./inventory.ini"
  }

  provisioner "local-exec" {
    command = "echo \"${join("\n", formatlist("%s ansible_host=%s etcd_member_name=%s", google_compute_instance.master.*.name, google_compute_instance.master.*.network_interface.0.access_config.0.nat_ip, google_compute_instance.master.*.name))}\" >> ./inventory.ini"
  }

  ##Create ETCD Inventory
  provisioner "local-exec" {
    command = "echo \"\n[etcd]\" >> ./inventory.ini"
  }

  provisioner "local-exec" {
    command = "echo \"${join("\n", google_compute_instance.master.*.name)}\" >> ./inventory.ini"
  }

  ##Create Nodes Inventory
  provisioner "local-exec" {
    command = "echo \"\n[kube-node]\" >> ./inventory.ini"
  }

  provisioner "local-exec" {
    command = "echo \"${join("\n", formatlist("%s ansible_host=%s", google_compute_instance.worker.*.name, google_compute_instance.worker.*.network_interface.0.access_config.0.nat_ip))}\" >> ./inventory.ini"
  }

  provisioner "local-exec" {
    command = "echo \"\n[k8s-cluster:children]\nkube-node\nkube-master\" >> ./inventory.ini"
  }
}

