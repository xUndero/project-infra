terraform {
  required_version = ">= 0.11.7"
}

provider "google" {
  version = "~> 3.23"
  project = var.project
  region  = var.region
}

resource "google_compute_instance" "balancer" {
  name         = "balancer"
  machine_type = "g1-small"
  zone         = "europe-west2-c"
  tags         = ["balancer"]

  metadata = {
    ssh-keys = "appuser:${file(var.public_key_path)}"
  }

  boot_disk {
    initialize_params {
      image = var.disk_image
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }
}

resource "google_compute_instance" "master" {
  count        = var.master_count
  name         = "etcd${count.index + 1}"
  machine_type = "n1-standard-2"
  zone         = var.zone

  metadata = {
    ssh-keys = "appuser:${file(var.public_key_path)}"
  }

  boot_disk {
    initialize_params {
      image = var.disk_image
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }
}

resource "google_compute_instance" "worker" {
  count        = var.worker_count
  name         = "node${count.index + 1}"
  machine_type = "n1-standard-1"
  zone         = var.zone

  metadata = {
    ssh-keys = "appuser:${file(var.public_key_path)}"
  }

  boot_disk {
    initialize_params {
      image = var.disk_image
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }
}

resource "google_compute_firewall" "k8s_nodeports" {
  name        = "k8s-nodeports"
  description = "My k8s rule"
  network     = "default"

  allow {
    protocol = "tcp"
    ports    = ["6443", "30000-32767"]
  }

  source_ranges = var.source_ranges
}

resource "google_compute_firewall" "balance_http" {
  name    = "allow-balance-default"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["balancer"]
}
