## Подготовка инфраструктуры

1. ### Для создания машин кластера используем Terraform:
  * Файл состояния разместим на Terraform Cloud;
  * Выполнение пока оставим на локальной машине;
  * Для создания inventory используем скрипт на terraform  
    (resource "null_resource" с рядом команд provisioner "local-exec");

2. ### Для развёртывания кластера возьмём Kubespray:
  * Из документации скопируем inventory: *`cp -rfp inventory/sample inventory/mycluster`*;
  * Чтоб в сертификат был добавлен внешний IP master-ноды:  
    добавим его в переменную *`supplementary_addresses_in_ssl_keys`*  
    в файле *`/inventory/mycluster/group_vars/k8s-cluster/k8s-cluster.yml`*;
  * А также включить опцию *`kubeconfig_localhost: true`* для копирования файла конфигурации  
    в локальный каталог (в полученном файле admin.conf в последствии следует изменить ip-адрес  
    на внешний);
